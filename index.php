<?php

include_once 'includes/header.php';

?>

    <!-- Page Content -->
    <div class="container">

<?php 

    if(!empty($_GET['page'])){
                
        $page = $_GET['page'] . ".php";
        
        $pages = scandir("pages", SCANDIR_SORT_ASCENDING);
        unset($pages[0], $pages[1]);
        //print_r($pages);
        
        if(in_array($page, $pages)){
            include_once 'pages/'.$page;
        }

    }
    else {
        include_once 'pages/home.php';
    }

?>

    </div>
    <!-- /.container -->
    
<?php 

include_once 'includes/footer.php';

?>