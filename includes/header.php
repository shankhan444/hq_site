<?php $base_url = "http://".$_SERVER['HTTP_HOST']."/"; 
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MLC Headquarter</title>

    <!-- Bootstrap core CSS -->
	<link href="<?php echo $base_url ?>css/main.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $base_url ?>css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.css">
	
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url ?>images/favicon-16x16.png">
    
    <script src="<?php echo $base_url ?>js/jquery-3.2.1.js" ></script>
    <script src="<?php echo $base_url ?>js/bootstrap.js"></script>
    
    <script type="text/javascript">
		$(document).ready(function(){
			
		});
			
    </script>

<style type="text/css">

</style>

</head>

<body>
  <div class="container">
  <!-- First Row Start -->
  <div class="row">
  <div class="col-lg-5 col-md-5">
    <div class="navbar-header">
      <a href="index.php?page=home"><img class="img-responsive" src="<?php echo $base_url ?>images/logo-3.png" /></a>
    </div>
    </div>
    
    <!-- Second Column Start -->
    
    <div class="col-lg-7 col-md-7 ma-top-right">
    	<div class="row">
			
			<div class="col-lg-12 pull-right">
				<ul class="social">
							<li>
                                <a href="#" class="search">
                                    <i class="fa fa-search"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="Facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="Twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="Linkedin">
                                    <i class="fa fa-youtube-play"></i>
                                </a>
                            </li>
                        </ul>
			</div>
    	</div>
    </div>
    
    <!-- Second Column End -->
    
    </div>
    
    <!-- First row end -->
    
    <!-- Second row start -->
    
    <nav class="navbar navbar-default center-nav-custom" style="margin-top: 1em">
    <div class="container">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
    	<div class="collapse navbar-collapse padding-nav-custom" id="myNavbar">
      <ul class="nav navbar-nav" style="display: inline-block; float: none;">
        
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">About Us <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?page=life_events">Overview</a></li>
            <li><a href="index.php?page=rest_house">Organogram</a></li>
            <li><a href="index.php?page=markets">Historical Perspective</a></li>
			<li><a href="index.php?page=legal-framework">Legal Frameworks</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">HQ Directorates <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?page=school">Directorate of HQ</a></li>
            <li><a href="index.php?page=library">Directorate of Lands</a></li>
			<li><a href="index.php?page=school">Directorate of Finance and Planning</a></li>
            <li><a href="index.php?page=library">Directorate of Cantt Administration</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Regions <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?page=parks#peshawar">Peshawar</a></li>
            <li><a href="index.php?page=parks#rawalpindi">Rawalpindi</a></li>
            <li><a href="index.php?page=parks#lahore">Lahore</a></li>
			<li><a href="index.php?page=parks#multan">Multan</a></li>
            <li><a href="index.php?page=parks#karachi">Karachi</a></li>
            <li><a href="index.php?page=parks#quetta">Quetta</a></li>
          </ul>
        </li>
        <li><a  href="index.php?page=news">News/ Events</a></li>
		<li><a  href="">Notifications</a></li>
		<li><a  href="">Downloads</a></li>
		<li><a  href="">Careers</a></li>
		<li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Contact Us <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="index.php?page=fire_brigade">Locations</a></li>
			<li><a href="index.php?page=fire_brigade">Feedback</a></li>		 
          </ul>
        </li>
      </ul>
    </div>
    </div>
    </nav>
    
    <!-- Second row end -->
    
  </div>