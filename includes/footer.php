<!-- Footer -->
<footer class="well jumbo-well-custom" style="padding: 5px;">
<div class="container">
<div class="row">
<div class="col-lg-4">
<p class="m-0 text-center text-white" style="margin-top: 5px;">Copyright &copy; 2017 ML&C Headquarter</p>
</div>
<div class="col-lg-4">

</div>
<div class="col-lg-4">
<p class="m-0 text-center text-white" style="margin-top: 5px;">Designed &amp; Developed by <i>CIMT</i></p>
</div>
</div>
</div>
<!-- /.container -->
</footer>

</body>

</html>
