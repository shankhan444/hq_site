      <!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2>About ML&C</h2>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        <i class="ion-ios-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="active">About</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- 
        ================================================== 
            Company Description Section Start
        ================================================== -->
		
		
		
        <section class="ma-legal-framwork-page">
            <div class="container">
                <div class="row">
					<div class="col-lg-6 col-md-6">
						<section class="panel panel-success">
						  <header class="panel-heading">
						   <h5 class="panel-title">Governing Statutes/ Ordinances</h5>
						  </header>
						  <div class="panel-body">
							<ul class="list-group">
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantonment Act,1924</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Land Acquisition Act, 1894</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantonment Rent Restriction Act, 1963</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantonment Pure Food Act, 1966</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Power of Attorney Act,1872</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Stamp Act, 1908</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Contract Act, 1872</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Civil Procedure Code, 1908</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Service Tribunal Act,1973</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Works of  Defence Act, 1903</a></li>
							  
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Transfer of Property Act, 1882</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantts Ordinance, 2002</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantts ( Immovable Property ) Ordinance,1948</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantts (Urban Immoveable Property Tax and  Entertainment Duty) Order,1979</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantts (Requisitioning of Immovable Property) Ordinance 1948</a></li>
							</ul>
						  </div>
						</section>
					</div>
					<div class="col-lg-6 col-md-6">
						<section class="panel panel-success">
						  <header class="panel-heading">
						   <h5 class="panel-title">Governing  Rules</h5>
						  </header>
						  <div class="panel-body">
							<ul class="list-group">
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cantt Land Admin (CLA) Rules 1937</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Acquisition, Custody, and Relinquishment (ACR) Rules 1944</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Cantt Servants Rules 1954  (Revised 2012)</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>Pakistan Cantts Accounts Code1955</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Pakistan Cantts Property Rules 1957</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantt Budget Rules, 1966</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Pakistan Cantts Cinematograph Rules ,1985</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantts Local Govts Election Rules, 2002</a></li>
							  
							</ul>
						  </div>
						</section>
					</div>
                </div>
            </div>
        </section>