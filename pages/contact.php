    <!-- Page Content -->
    <div class="container">

        <!-- Heading Row -->
        <div class="row my-4">
            <div class="col-lg-6">
            <h3>Submit a Query</h3>
            <hr>
                <form action="">
                <div class="form-group">
                <label for="name">Name: </label>
                <input type="text" name="name" id="name" class="form-control">
                </div>
                <div class="form-group">
                <label for="Phone">Phone: </label>
                <input type="tel" name="phone" id="phone" class="form-control">
                </div>
                <div class="form-group">
                <label for="email">Email: </label>
                <input type="email" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                <label for="message">Message: </label>
                <textarea rows="5" cols="50" class="form-control"></textarea>
                </div>
                <div class="form-group">
                <input type="submit" class="form-control" style="width: 50%">
                </div>
                </form>
            </div>
            <!-- /.col-lg-6 -->
            <div class="col-lg-6" style="margin-bottom: 20px;">
                <h3>Location</h3>
                <hr>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3323.3961249503477!2d73.05657621482123!3d33.595025080732825!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38df949e7b98a8db%3A0xd12eda09bf1f6aac!2sChaklala+Cantonment+Board!5e0!3m2!1sen!2s!4v1502884078273" width="100%" height="450" style="border:0"></iframe>
            </div>
            <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->

        <!-- Content Row -->

        <!-- /.row -->

    </div>
    <!-- /.container -->