      <!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2>About ML&C</h2>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        <i class="ion-ios-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="active">About</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- 
        ================================================== 
            Company Description Section Start
        ================================================== -->
		
		
		
        <section class="company-description">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="block">
                            <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Historical Perspective</h3>
                            <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                                With the establishment of East India Company, origin of Lands & Cantonments Department dates back to 1658 in Sub Continent and refined in 1857. Cantonments were established in various parts of India by East India Company after the battle of Plassey in 1757. The first Cantonment was established in 1765 at Barrackpore, near Calcutta.  The  Cantonments Act was promulgated in 1924. Cantonments were developed by British rule all over Sub Continent (India, Pakistan, Bangladesh & Sri Lanka). It is an exclusively distinct civil-military hybrid administration model. Pakistan inherited 23 cantonments. 
                            </p>
                            <p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                                The Military Lands and Cantonments Service as a Civil Service was created in 1940's. At present ML&C cadre has 84 Officers. There are 44 Cantonments and 11 Military Estate Office Circles. The department looks after the civic functions in the Cantoments through Cantonment Boards and each Cantonment has a Chief Executive Officer from Military Lands and Cantonments Service. 
                            </p>
							<p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                                Military Estates Officers look after the requirements of the local military authorities for land and buildings as well as manage defence lands. All land acquisitions for all the services under Ministry of Defence are carried out by ML&C Deptt through Military Estates Offices. All defence land records are maintained by the Military Estates Offices. 
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>