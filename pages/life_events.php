
        <!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2>About ML&C</h2>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        <i class="ion-ios-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="active">About</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- 
        ================================================== 
            Company Description Section Start
        ================================================== -->
		
		
		
        <section class="company-description">
            <div class="container">
				<div class="row">
					<img class="img-responsive" src="images/about/mission.jpeg" style="width:60%; margin:0 auto; margin-bottom: 30px;" />
					
				</div>
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                        <img src="images/about.png" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Overview</h3>
                            <p  class="wow fadeInUp" data-wow-delay=".5s" data-wow-duration="500ms">
                                Directorate of Military Lands and Cantonments is Headquarter of Military Lands and Cantonments Department. HQ ML&C provides advisory input on defence lands and local government matters in cantonments to the Federal Government (Ministry of Defence) and all related stakeholders i.e. Service Headquarters and other organizations under Ministry of Defence. 
                            </p>
                            <p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
                                Ministry of Defence heads ML&C Dept as Federal Govt with its powers and authority particularly in Land matters, Financial sanctions and HR management. Acquisition or requisitioning of property for Defence Services, delimitation of cantonment areas, Local self-government in cantonment areas are some of the responsibilities of HQ ML&C. It also ensures implementation of Cantonment Act 1924, Policies, Executive instructions and all relevant Rules & Regulations. 
                            </p>
                            
                        </div>
                    </div>
                </div>
				<div class="row">
					<div class="col-md-6">
						<br />
						<h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">ML&C Offices Over Map</h3>
						<p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
									ML&C Department serves an area of about 6 hundred thousand acres of land, attends to and promotes the interest of more than 5 million people, handles budgets amounting to approximately Rs. 15 billion which are increasing on yearly basis and manages a human resource base of nearly 22 thousand employees.
						</p>
						<br />
						<p  class="wow fadeInUp" data-wow-delay=".7s" data-wow-duration="500ms">
									Military Lands & Cantonment Department is headed by the Director General, Military Lands & Cantonments(DG ML&C). He is followed in hierarchy by an Additional Director General and assisted by four Directors in HQ. HQ ML&C has under its jurisdiction six regional directorates namely, Peshawar, Rawalpindi, Lahore, Multan, Karachi and Quetta. HQ ML&C through its Regional Directorates manages 11 Military Estate(ME) Circles and 44 Cantonment Boards (CBs) in all over Pakistan 
									Organisational structure of HQ ML&C is <a href="#">here</a>
						</p>
					</div>
					<div class="col-md-6 wow fadeInLeft" data-wow-delay=".3s" >
                        <img src="images/about/map.jpeg" alt="" class="img-responsive">
                    </div>
				</div>
            </div>
        </section>


        <!-- 
        ================================================== 
            Company Feature Section Start
        ================================================== -->
        <section class="about-feature clearfix">
            <div class="container-fluid">
                <div class="row">
				<h3 class="subtitle wow fadeInUp" data-wow-delay=".3s" data-wow-duration="500ms">Roles & Responsibilities</h3>
                    <div class="block about-feature-1 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".3s">
                        <h2>
                        President
                        </h2>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.
                        </p>
                    </div>
                    <div class="block about-feature-2 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".5s">
                        <h2 class="item_title">
                        CEO
                        </h2>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.
                        </p>
                    </div>
                    <div class="block about-feature-3 wow fadeInDown" data-wow-duration="500ms" data-wow-delay=".7s">
                        <h2 class="item_title">
                        MEO
                        </h2>
                        <p>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.
                        </p>
                    </div>
                </div>
            </div>
        </section>

