<div class="container">

	<div class="row">

		<div class="col-lg-12 col-md-12">
			<h2 class="pagename-custom">CB Schools and Colleges</h2>
			<hr>
		</div>

	</div>

		<!-- Carousel Start -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 20px;">
    <!-- Carousel indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>   
    <!-- Wrapper for carousel items -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo $base_url ?>images/education/school/slider/1.jpg" alt="First Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/2.jpg" alt="Second Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/3.jpg" alt="Third Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/4.jpg" alt="Third Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/education/school/slider/5.jpg" alt="Third Slide">
        </div>
    </div>
    <!-- Carousel controls -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
		<!-- Carousel End -->

<div class="jumbotron jumbo-well-custom">
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem magni quas ex numquam, maxime minus quam molestias corporis quod, ea minima accusamus.
</div>

</div>