      <!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2>News & Events</h2>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        <i class="ion-ios-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="active">News</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- 
        ================================================== 
            Company Description Section Start
        ================================================== -->
		
		
		
        <section class="ma-legal-framwork-page">
            <div class="container">
                <div class="row">
					<div class="col-lg-12">
						<section class="panel panel-success">
						  <header class="panel-heading">
						   <h5 class="panel-title">News & Events</h5>
						  </header>
						  <div class="panel-body">
							<ul class="list-group">
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>DG ML&C visited Cantonment Institute of Management and Land Administration (CIMLA), Lahore on 24 August, 2017</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>CEOs Conference was held in Board Room of Chaklala Cantonment Board on 25 August, 2017 under chairmanship of DG ML&C. Officers of HQ ML&C and CEOs of CBs in Education Project Phase - II attended the conference.</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantonment Rent Restriction Act, 1963</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Cantonment Pure Food Act, 1966</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Power of Attorney Act,1872</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Stamp Act, 1908</a></li>
							  <li class="list-group-item"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i>The Contract Act, 1872</a></li>
							  
							</ul>
						  </div>
						</section>
					</div>
                </div>
            </div>
        </section>