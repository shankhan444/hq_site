
        <!-- 
        ================================================== 
            Global Page Section Start
        ================================================== -->
        <section class="global-page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <h2>Cantonment Boards And MEO's</h2>
                            <ol class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        <i class="ion-ios-home"></i>
                                        Home
                                    </a>
                                </li>
                                <li class="active">Regions</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>   
        </section><!--/#Page header-->

        <!-- 
        ================================================== 
            Service Page Section  Start
        ================================================== -->
        <!--<section id="service-page" class="pages service-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h2 class="subtitle wow fadeInUp animated" data-wow-delay=".3s" data-wow-duration="500ms">What We Love To Do</h2>
                            <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" data-wow-duration="500ms">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis porro recusandae non quibusdam iure adipisci.</p>
                            <div class="row service-parts">
                                <div class="col-md-6">
                                    <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="600ms">
                                        <i class="ion-ios-paper-outline"></i>
                                        <h4>BRANDING</h4>
                                        <p>Veritatis eligendi, dignissimos. Porta fermentum mus aute pulvinar earum minus platea massa feugiat rutrum urna facilisi ipsameum.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="800ms">
                                        <i class="ion-ios-pint-outline"></i>
                                        <h4>DESIGN</h4>
                                        <p>Veritatis eligendi, dignissimos. Porta fermentum mus aute pulvinar earum minus platea massa feugiat rutrum urna facilisi ipsameum.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="1s">
                                        <i class="ion-ios-paper-outline"></i>
                                        <h4>DEVELOPMENT</h4>
                                        <p>Veritatis eligendi, dignissimos. Porta fermentum mus aute pulvinar earum minus platea massa feugiat rutrum urna facilisi ipsameum.</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="block wow fadeInUp animated" data-wow-duration="400ms" data-wow-delay="1.1s">
                                        <i class="ion-ios-paper-outline"></i>
                                        <h4>THEMEING</h4>
                                        <p>Veritatis eligendi, dignissimos. Porta fermentum mus aute pulvinar earum minus platea massa feugiat rutrum urna facilisi ipsameum.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <img class="img-responsive" src="images/regions.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- 
        ================================================== 
            Region Section Start
        ================================================== -->
        <section class="works service-page region-page">

            <div class="container" >
                <h2 class="subtitle wow fadeInUp animated sk-heading" data-wow-delay=".3s" data-wow-duration="500ms" id="peshawar" style="cursor: pointer" onclick="regionPopulator(this.id)" >Peshawar Region
                    <p class="text-danger glyphicon glyphicon-plus pull-right" ></p>
                </h2>

                <div class="row peshawar-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        Cantonments
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Peshawar
                                    </a>
                                </h4>
                                <p>
                                    Peshawar Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Nowshera
                                    </a>
                                </h4>
                                <p>
                                    Nowshera Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Kohat
                                    </a>
                                </h4>
                                <p>
                                    Kohat Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Bannu
                                    </a>
                                </h4>
                                <p>
                                    Bannu Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Mardan
                                    </a>
                                </h4>
                                <p>
                                    Mardan Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Risalpur
                                    </a>
                                </h4>
                                <p>
                                    Risalpur Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Cherat
                                    </a>
                                </h4>
                                <p>
                                    Cherat Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        D.I. Khan
                                    </a>
                                </h4>
                                <p>
                                    D.I. Khan Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Swat
                                    </a>
                                </h4>
                                <p>
                                    Swat Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>

                <div class="row peshawar-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        MEO's
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Peshawar
                                    </a>
                                </h4>
                                <p>
                                    Peshawar MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        MEO Kohat
                                    </a>
                                </h4>
                                <p>
                                    MEO Kohat
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="container" >
                <h2 class="subtitle wow fadeInUp animated sk-heading" data-wow-delay=".3s" style="cursor: pointer" data-wow-duration="500ms" id="rawalpindi" onclick="regionPopulator(this.id)" >Rawalpindi Region
                    <p class="text-danger glyphicon glyphicon-plus pull-right" ></p>
                </h2>
                <div class="row rawalpindi-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        Cantonments
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Rawalpindi
                                    </a>
                                </h4>
                                <p>
                                    Rawalpindi Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Chaklala
                                    </a>
                                </h4>
                                <p>
                                    Chaklala Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Wah
                                    </a>
                                </h4>
                                <p>
                                    Wah Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Abbottabad
                                    </a>
                                </h4>
                                <p>
                                    Abbottabad Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Attok
                                    </a>
                                </h4>
                                <p>
                                    Attok Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Taxila
                                    </a>
                                </h4>
                                <p>
                                    Taxila Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Havelian
                                    </a>
                                </h4>
                                <p>
                                    Havelian Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Kamra
                                    </a>
                                </h4>
                                <p>
                                    Kamra Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Murree Hills
                                    </a>
                                </h4>
                                <p>
                                    Murree Hills Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Murree Galis
                                    </a>
                                </h4>
                                <p>
                                    Murree Galis Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Sanjwal
                                    </a>
                                </h4>
                                <p>
                                    Sanjwal Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="row rawalpindi-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        MEO's
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Rawalpindi
                                    </a>
                                </h4>
                                <p>
                                    Rawalpindi MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Abbottabad
                                    </a>
                                </h4>
                                <p>
                                    Abbottabad MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="container" >
                <h2 class="subtitle wow fadeInUp animated sk-heading" data-wow-delay=".3s" style="cursor: pointer" data-wow-duration="500ms" id="lahore" onclick="regionPopulator(this.id)" >Lahore Region
                    <p class="text-danger glyphicon glyphicon-plus pull-right" ></p>
                </h2>
                <div class="row lahore-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        Cantonments
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Lahore
                                    </a>
                                </h4>
                                <p>
                                    Lahore Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Walton
                                    </a>
                                </h4>
                                <p>
                                    Walton Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Gujranwala
                                    </a>
                                </h4>
                                <p>
                                    Gujranwala Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Sialkot
                                    </a>
                                </h4>
                                <p>
                                    Sialkot Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Jhelum
                                    </a>
                                </h4>
                                <p>
                                    Jhelum Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Kharian
                                    </a>
                                </h4>
                                <p>
                                    Kharian Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Mangla
                                    </a>
                                </h4>
                                <p>
                                    Mangla Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="row lahore-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        MEO's
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Lahore
                                    </a>
                                </h4>
                                <p>
                                    Lahore MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Gujranwala
                                    </a>
                                </h4>
                                <p>
                                    Gujranwala MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="container" >
                <h2 class="subtitle wow fadeInUp animated sk-heading" data-wow-delay=".3s" style="cursor: pointer" data-wow-duration="500ms" id="multan" onclick="regionPopulator(this.id)" >Multan Region
                    <p class="text-danger glyphicon glyphicon-plus pull-right" ></p>
                </h2>
                <div class="row multan-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        Cantonments
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Multan
                                    </a>
                                </h4>
                                <p>
                                    Multan Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Sargodha
                                    </a>
                                </h4>
                                <p>
                                    Sargodha Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Bahawalpur
                                    </a>
                                </h4>
                                <p>
                                    Bahawalpur Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Shorkot
                                    </a>
                                </h4>
                                <p>
                                    Shorkot Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Okara
                                    </a>
                                </h4>
                                <p>
                                    Okara Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="row multan-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        MEO's
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Multan
                                    </a>
                                </h4>
                                <p>
                                    Multan MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Sargodha
                                    </a>
                                </h4>
                                <p>
                                    Sargodha MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="container" >
                <h2 class="subtitle wow fadeInUp animated sk-heading" data-wow-delay=".3s" style="cursor: pointer" data-wow-duration="500ms" id="karachi" onclick="regionPopulator(this.id)" >Karachi Region
                    <p class="text-danger glyphicon glyphicon-plus pull-right" ></p>
                </h2>
                <div class="row karachi-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        Cantonments
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Karachi
                                    </a>
                                </h4>
                                <p>
                                    Karachi Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Clifton
                                    </a>
                                </h4>
                                <p>
                                    Clifton Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Hyderabad
                                    </a>
                                </h4>
                                <p>
                                    Hyderabad Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Faisal
                                    </a>
                                </h4>
                                <p>
                                    Faisal Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Malir
                                    </a>
                                </h4>
                                <p>
                                    Malir Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Malir
                                    </a>
                                </h4>
                                <p>
                                    Malir Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Korangi Creek
                                    </a>
                                </h4>
                                <p>
                                    Korangi Creek Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Manora
                                    </a>
                                </h4>
                                <p>
                                    Manora Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Pano Aqil
                                    </a>
                                </h4>
                                <p>
                                    Pano Aqil Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="row karachi-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        MEO's
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Karachi
                                    </a>
                                </h4>
                                <p>
                                    Karachi MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Hyderabad
                                    </a>
                                </h4>
                                <p>
                                    Hyderabad MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="container" >
                <h2 class="subtitle wow fadeInUp animated sk-heading" data-wow-delay=".3s" style="cursor: pointer" data-wow-duration="500ms" id="quetta" onclick="regionPopulator(this.id)" >Quetta Region
                    <p class="text-danger glyphicon glyphicon-plus pull-right" ></p>
                </h2>
                <div class="row quetta-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        Cantonments
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Quetta
                                    </a>
                                </h4>
                                <p>
                                    Quetta Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Zhob
                                    </a>
                                </h4>
                                <p>
                                    Zhob Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Loralai
                                    </a>
                                </h4>
                                <p>
                                    Loralai Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Ormara
                                    </a>
                                </h4>
                                <p>
                                    Ormara Cantonment Board
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="row quetta-region" hidden>
                    <p class="subtitle-des wow fadeInUp animated" data-wow-delay=".5s" style="font-weight: bolder" data-wow-duration="500ms">
                        MEO's
                    </p>
                    <div class="col-sm-3">
                         <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                            <div class="img-wrapper">
                                <img src="images/portfolio/item-1.jpg" class="img-responsive" alt="this is a title" >
                                <div class="overlay">
                                    <div class="buttons">
                                        <a rel="gallery" class="fancybox" href="images/portfolio/item-1.jpg">Go To Website</a>
                                    </div>
                                </div>
                            </div>
                            <figcaption>
                                <h4>
                                    <a href="#">
                                        Quetta
                                    </a>
                                </h4>
                                <p>
                                    Quetta MEO
                                </p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </section>




<script>
    function regionPopulator(regionName)
    {
        var select=regionName+"-region";
        $("."+select).toggle();
    }
</script>