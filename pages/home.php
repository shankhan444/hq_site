		<!-- Carousel Start -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 20px;">
    <!-- Carousel indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>   
    <!-- Wrapper for carousel items -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="<?php echo $base_url ?>images/home-slider/slide1.jpeg" alt="First Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/home-slider/slide2.jpeg" alt="Second Slide">
        </div>
        <div class="item">
            <img src="<?php echo $base_url ?>images/home-slider/slide3.jpeg" alt="Third Slide">
        </div>
		<div class="item">
            <img src="<?php echo $base_url ?>images/home-slider/slide4.jpeg" alt="Third Slide">
        </div>
		<div class="item">
            <img src="<?php echo $base_url ?>images/home-slider/slide5.jpeg" alt="Third Slide">
        </div>
    </div>
    <!-- Carousel controls -->
    <a class="carousel-control left" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="carousel-control right" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
		<!-- Carousel End -->

		<!--
            ==================================================
            Message Section Start
            ================================================== -->
			<!-- Two Section -->
			<section id="blog-full-width">
				<div class="container">
					<div class="row">
						<div class="col-md-8">
							<div class="section-heading ma-overview">
							<h1 class="title wow fadeInDown" data-wow-delay=".3s">ML&C Overview</h1>
							<p class="wow fadeInDown" data-wow-delay=".5s">
                            The Military Lands & Cantonments Department came into being in its present form through the promulgation of Cantonment Act, 1924. The Department is required to manage the Federal Government Estates and to extend local government in various cantonments. The ML&C Department serves an area of about 6 hundred thousand acres of land, attends to and promotes the interests of more than 5 million people, handles budgets amounting to approximately Rs. 15 billion which are increasing on yearly basis and manages a human resource base of nearly 22 thousand employees.
							<br /> <br />
							The Military Lands & Cantonment Department is headed by the Director General, Military Lands & Cantonments. He is followed in hierarchy by an Additional Director General, assisted by six Regional Directors (i.e. Karachi, Quetta, Lahore, Multan, Rawalpindi & Peshawar) and four Directors in HQ. They in turn are followed by Cantonment Executive Officers and Military Estates Officers. The Department through its Regional HQs manages 43 Cantonment Boards and 11 ME Circles.

							</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="sidebar">	
									<div class="recent-post widget">
                                            <ul class="nav nav-tabs">
                                                <li class="active">
                                                    <a  href="#quick-links" data-toggle="tab">Quick Links</a>
                                                </li>
												<li>
                                                    <a  href="#news" data-toggle="tab">News</a>
                                                </li>
                                                <li>
													<a href="#notifications" data-toggle="tab">Notifications</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content ">
                                                <div class="tab-pane active" id="quick-links">
                                                    <ul>
                                                        <li>
                                                            <a href="#">Corporate meeting turns into a photoshooting.</a><br>
                                                            <time>16 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Statistics,analysis. The key to succes.</a><br>
                                                            <time>15 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Blog post without image, only text.</a><br>
                                                            <time>14 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Blog post with audio player. Share your creations.</a><br>
                                                            <time>14 May, 2015</time>
                                                        </li>
                                                    </ul>
                                                </div>
												<div class="tab-pane" id="news">
                                                    <ul>
                                                        <li>
                                                            <a href="#">Corporate meeting turns into a photoshooting.</a><br>
                                                            <time>16 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Statistics,analysis. The key to succes.</a><br>
                                                            <time>15 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Blog post without image, only text.</a><br>
                                                            <time>14 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Blog post with audio player. Share your creations.</a><br>
                                                            <time>14 May, 2015</time>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="tab-pane" id="notifications">
                                                    <ul>
                                                        <li>
                                                            <a href="#">The Military Lands & Cantonments Department came into being in its present form through </a><br>
                                                            <time>16 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Statistics,analysis. The key to succes.</a><br>
                                                            <time>15 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Blog post without image, only text.</a><br>
                                                            <time>14 May, 2015</time>
                                                        </li>
                                                        <li>
                                                            <a href="#">Blog post with audio player. Share your creations.</a><br>
                                                            <time>14 May, 2015</time>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
									</div>
									
								</div>
							</div>
						</div>
					</section>
					
					<section id="works" class="works">
                <div class="container">
                   <!-- <div class="section-heading">
                         <h1 class="title wow fadeInDown" data-wow-delay=".3s">Latest Works</h1>
                    </div> --> 
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <figure class="wow fadeInLeft animated portfolio-item" data-wow-duration="500ms" data-wow-delay="0ms">
                                <div class="img-wrapper">
                                    <img src="images/profiles/sd.jpg" class="ma-message-img img-responsive" alt="this is a title" >
                                    <p class="ma-message-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, sint.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, sint.</p>
									<div class="overlay">
                                        <div class="buttons">
                                            <a target="_blank" href="single-portfolio.html">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <figcaption>
                                <h4>
                                <a href="#">
                                    Secretary Defence
                                </a>
                                </h4>
                                <p>
                                    Lt. Gen Zamir Ul Hassan Shah HI(M)(Retd)
                                </p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                                <div class="img-wrapper">
                                    <img src="images/profiles/dg_mlc.jpg" class="ma-message-img img-responsive" alt="this is a title" >
									<p class="ma-message-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, sint.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, sint.</p>
                                    <div class="overlay">
                                        <div class="buttons">
                                            <a target="_blank" href="single-portfolio.html">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <figcaption>
                                <h4>
                                <a href="#">
                                    DG ML&C
                                </a>
                                </h4>
                                <p>
                                    Major General Syed Najmul Hassan Shah
                                </p>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <figure class="wow fadeInLeft animated" data-wow-duration="500ms" data-wow-delay="300ms">
                                <div class="img-wrapper">
                                    <img src="images/profiles/addl_dg.jpg" class="ma-message-img img-responsive" alt="" >
                                    <p class="ma-message-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, sint.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum, sint.</p>
									<div class="overlay">
                                        <div class="buttons">
                                            <a target="_blank" href="single-portfolio.html">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <figcaption>
                                <h4>
                                <a href="#">
                                    Addl: DG ML&C
                                </a>
                                </h4>
                                <p>
                                    Mr. Hamid Haroon
                                </p>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </section> <!-- #works -->
		